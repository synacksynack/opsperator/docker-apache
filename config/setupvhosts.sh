APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
SSL_INCLUDE=${SSL_INCLUDE:-no-ssl}
if test -s /etc/apache2/sites-enabled/000-servername.conf; then
    echo Skipping ServerName generation - already initialized
else
    echo Initializing ServerName
    if test -z "$APACHE_SERVERNAME"; then
	APACHE_SERVERNAME=`hostname -s`
    fi
    echo ServerName $APACHE_SERVERNAME \
	>/etc/apache2/sites-enabled/000-servername.conf
fi

if test -s /etc/apache2/sites-enabled/001-listen.conf; then
    echo Skipping Bind Ports generation - already initialized
else
    echo Initializing Bind Ports
    echo Listen $APACHE_HTTP_PORT \
	>/etc/apache2/sites-enabled/001-listen.conf
fi

if test -s /etc/apache2/sites-enabled/003-vhosts.conf; then
    echo Skipping Virtualhosts generation - already initialized
elif test -d /vhosts; then
    echo Installing Custom Virtualhosts
    find /vhosts -name '*.conf' | while read conf
	do
	    sed -e "s|APACHE_DOMAIN|$APACHE_DOMAIN|g" \
		-e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
		-e "s|SSL_INCLUDE|$SSL_INCLUDE.conf|g" "$conf"
	done >/etc/apache2/sites-enabled/003-vhosts.conf
else
    echo No VirtualHosts templates to install
fi

test "$SSL_INCLUDE" = do-ssl || SSL_INCLUDE=no-ssl
sed -e "s|HTTP_PORT|$APACHE_HTTP_PORT|g" \
    -e "s|SSL_INCLUDE|$SSL_INCLUDE.conf|g" /status.conf \
    >/etc/apache2/sites-enabled/999-status.conf
