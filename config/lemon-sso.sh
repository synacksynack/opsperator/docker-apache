if test "$APACHE_IGNORE_OPENLDAP" -o "$APACHE_IGNORE_AUTH"; then
    echo Skipping Authentication Configuration - Ignored by Runtime
else
    APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
    if test "$OPENLDAP_HOST" -a "$OPENLDAP_DOMAIN"; then
	OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=apache,ou=services}"
	OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
	OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
	OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
	OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
	if test -z "$OPENLDAP_BASE"; then
	    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
	fi
	if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
	    OPENLDAP_PORT=636
	elif test -z "$OPENLDAP_PORT"; then
	    OPENLDAP_PORT=389
	fi
	echo Waiting for LDAP backend ...
	cpt=0
	while true
	do
	    if LDAPTLS_REQCERT=never \
		   ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		    -D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		    -b "ou=users,$OPENLDAP_BASE" \
		    -w "$OPENLDAP_BIND_PW" \
		    "(objectClass=$OPENLDAP_USERS_OBJECTCLASS)" \
		    >/dev/null 2>&1; then
		echo " LDAP is alive!"
		break
	    elif test "$cpt" -gt 25; then
		echo Could not reach OpenLDAP >&2
		exit 1
	    fi
	    echo LDAP is ... KO
	    sleep 5
	    cpt=`expr $cpt + 1`
	done
    fi
    if test "$AUTH_METHOD" = oidc; then
	if test "$OIDC_SKIP_TLS_VERIFY"; then
	    sed -i \
		-e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer Off|' \
		-e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer Off|' \
		/etc/apache2/mods-enabled/auth_openidc.conf
	else
	    sed -i \
		-e 's|^#*[ ]*OIDCSSLValidateServer.*|OIDCSSLValidateServer On|' \
		-e 's|^#*[ ]*OIDCValidateIssuer.*|OIDCValidateIssuer On|' \
		/etc/apache2/mods-enabled/auth_openidc.conf
	fi
	sed -i \
	    -e 's|^#*[ ]*OIDCPassClaimsAs.*|OIDCPassClaimsAs environment|' \
	    -e 's|^#*[ ]*OIDCProviderAuthRequestMethod.*|OIDCProviderAuthRequestMethod GET|' \
	    -e 's|^#*[ ]*OIDCUserInfoTokenMethod.*|OIDCUserInfoTokenMethod authz_header|' \
	    -e 's|^#*[ ]*OIDCSessionCookieChunkSize.*|OIDCSessionCookieChunkSize 4000|' \
	    -e 's|^#*[ ]*OIDCSessionType.*|OIDCSessionType client-cookie|' \
	    -e 's|^#*[ ]*OIDCStripCookies.*|OIDCStripCookies mod_auth_openidc_session mod_auth_openidc_session_chunks mod_auth_openidc_session_0 mod_auth_openidc_session_1|' \
	    /etc/apache2/mods-enabled/auth_openidc.conf
	echo LemonLDAP-NG OIDC Configuration Initialized
    elif ! test "$AUTH_METHOD" = lemon; then
	echo Skipping LemonLDAP Configuration - auth:$AUTH_METHOD
    elif test -s /etc/lemonldap-ng/lemonldap-ng.ini; then
	echo Skipping LemonLDAP Configuration - already initialized
    elif test "$OPENLDAP_HOST" -a "$OPENLDAP_DOMAIN"; then
	sed -e "s LDAP_PROTO $OPENLDAP_PROTO g" \
	    -e "s LDAP_HOST $OPENLDAP_HOST g" \
	    -e "s LDAP_PORT $OPENLDAP_PORT g" \
	    -e "s|LDAP_SUFFIX|$OPENLDAP_BASE|g" \
	    -e "s|LDAP_CONF_DN_PREFIX|$OPENLDAP_CONF_DN_PREFIX|g" \
	    -e "s|LDAP_BIND_DN_PREFIX|$OPENLDAP_BIND_DN_PREFIX|g" \
	    -e "s|LDAP_BIND_PW|$OPENLDAP_BIND_PW|g" \
	    /usr/share/lemon/etc-lemonldap-ng/lemonldap-ng.ini \
	    >/etc/lemonldap-ng/lemonldap-ng.ini
	echo LemonLDAP-NG Handler Configuration Initialized
    else
	echo Skipping Authentication Configuration - no backend defined
    fi
fi
