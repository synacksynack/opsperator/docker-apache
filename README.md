# k8s Apache

Generic Apache image, LLNG2-ready, forked from
https://github.com/Worteks/docker-apache

Build with:

```
$ make build
```

Test with:

```
$ make run
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name              |    Description             | Default                                                            |
| :---------------------------- | -------------------------- | ------------------------------------------------------------------ |
|  `APACHE_DOMAIN`              | Apache Main VirtualHost    | `example.com`                                                      |
|  `APACHE_SERVERNAME`          | Apache ServerName          | container `hostname -s`                                            |
|  `APACHE_IGNORE_OPENLDAP`     | Ignore LemonLDAP autoconf  | undef                                                              |
|  `APACHE_HTTP_PORT`           | Apache Listen Port         | `8080`                                                             |
|  `AUTH_METHOD`                | Apache Auth Method         | `lemon`, could be `lemon`, `ldap`, `oidc` or `none`                |
|  `ONLY_TRUST_KUBE_CA`         | Don't trust base image CAs | `false`, any other value disables ca-certificates CAs              |
|  `OPENLDAP_BASE`              | OpenLDAP Base              | seds `OPENLDAP_DOMAIN`, `example.com` produces `dc=example,dc=com` |
|  `OPENLDAP_BIND_DN_RREFIX`    | OpenLDAP Bind DN Prefix    | `cn=apache,ou=services`                                            |
|  `OPENLDAP_BIND_PW`           | OpenLDAP Bind Password     | `secret`                                                           |
|  `OPENLDAP_CONF_DN_RREFIX`    | OpenLDAP Conf DN Prefix    | `cn=lemonldap,ou=config`                                           |
|  `OPENLDAP_DOMAIN`            | OpenLDAP Domain Name       | undef                                                              |
|  `OPENLDAP_HOST`              | OpenLDAP Backend Address   | undef                                                              |
|  `OPENLDAP_PORT`              | OpenLDAP Bind Port         | `389` or `636` depending on `OPENLDAP_PROTO`                       |
|  `OPENLDAP_PROTO`             | OpenLDAP Proto             | `ldap`                                                             |
|  `OPENLDAP_USERS_OBJECTCLASS` | OpenLDAP Users ObjectClass | `inetOrgPerson`                                                    |
|  `PUBLIC_PROTO`               | Apache Public Proto        | `http`                                                             |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                                                             |
| :------------------ | ----------------------------------------------------------------------- |
|  `/certs`           | Apache Certificate (optional)                                           |
|  `/vhosts`          | Apache VirtualHosts templates root - processed during container start   |
