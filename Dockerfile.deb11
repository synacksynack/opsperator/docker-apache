FROM docker.io/debian:bullseye-slim

# Apache Base image for OpenShift Origin

LABEL io.k8s.description="Apache 2.4 Base Image." \
      io.k8s.display-name="Apache 2.4" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="apache,httpd,apache2,apache24,llng,llng2" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-apache" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="2.4"

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    DUMMY_DH_SIZE=2048 \
    PERL5LIB=/usr/share/perl5

COPY config/* /

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install vim; \
    fi \
    && echo "# Install LLNG2 Repository" \
    && apt-get -y install gnupg ca-certificates \
    && cat /lemonldap-ng.key | apt-key add - \
    && mv /lemonldap-ng.list /etc/apt/sources.list.d/ \
    && apt-get -y update \
    && echo "# Install Apache" \
    && apt-get install --no-install-recommends -y apache2 apache2-data \
	libnss-wrapper ldap-utils libmodule-build-perl build-essential \
	libapache2-mod-perl2 libcgi-pm-perl libapache2-mod-auth-openidc \
	libapache-session-ldap-perl libdbd-pg-perl apache2-utils \
	lemonldap-ng-handler cpanminus libredis-perl \
    && echo "# Pins LemonLDAP-NG Handler Version" \
    && ( \
	echo "# Added by docker-apache" \
	&& echo Package: lemonldap-ng-handler \
	&& echo Pin: release n=main \
	&& echo Pin-Priority: 942 \
    ) >>/etc/apt/preferences \
    && echo "# LemonLDAP-NG Browseable Sessions Fix" \
    && apt-get -y remove --purge libapache-session-browseable-perl \
    && ( \
	echo y; \
	echo o conf prerequisites_policy follow; \
	echo o conf mbuild_arg --install_base $PERL5LIB; \
	echo o conf makepl_arg PREFIX=$PERL5LIB INSTALLDIRS=site; \
	echo o conf mbuildpl_arg --install_base $PERL5LIB --installdirs site; \
	echo o conf commit \
    ) | cpan \
    && cpan install \
	Apache::Session::Postgres \
	Apache::Session::Browseable \
	Apache::Session::Browseable::LDAP \
	Apache::Session::Browseable::PgJSON \
	Apache::Session::Browseable::Postgres \
	Apache::Session::Browseable::Redis \
	Apache::Session::Browseable::Store::LDAP \
    && find /.cpan/build/*/lib /root/.cpan/build/*/lib -type f 2>/dev/null \
	| while read fl; \
	    do \
		dest=`echo $fl | sed 's|^.*cpan/build/[^/]*/lib/||'` \
		&& basedir=`dirname $dest` \
		&& mkdir -p $PERL5LIB/$basedir \
		&& cp -pv $fl $PERL5LIB/$dest; \
	    done \
    && find /.cpan/build/*/blib/lib /root/.cpan/build/*/blib/lib -type f \
	2>/dev/null | while read fl; \
	    do \
		dest=`echo $fl | sed 's|^.*cpan/build/[^/]*/blib/lib/||'` \
		&& basedir=`dirname $dest` \
		&& mkdir -p $PERL5LIB/$basedir \
		&& cp -pv $fl $PERL5LIB/$dest; \
	    done \
    && for lib in LDAP Redis Postgres pgJSON; do \
	mkdir -p $PERL5LIB/auto/Apache/Session/Browseable/$lib \
	&& ln -sf $PERL5LIB/auto/Apache/Session/Browseable/_common \
	    $PERL5LIB/auto/Apache/Session/Browseable/$lib/SUPER; \
    done \
    && chown -R root:root $PERL5LIB \
    && mkdir -p /usr/share/lemon/etc-lemonldap-ng \
    && mv /lemonldap-ng.ini /usr/share/lemon/etc-lemonldap-ng/ \
    && echo "# Configuring Apache" \
    && mv /envvars /etc/apache2/envvars \
    && . /etc/apache2/envvars \
    && ln -sfT /dev/stderr "$APACHE_LOG_DIR/error.log" \
    && ln -sfT /dev/stdout "$APACHE_LOG_DIR/access.log" \
    && ln -sfT /dev/stdout "$APACHE_LOG_DIR/other_vhosts_access.log" \
    && sed -i '/[Ii]nclude.*ports.conf/d' /etc/apache2/apache2.conf \
    && if grep ErrorLog /etc/apache2/apache2.conf >/dev/null; then \
	sed -i 's|ErrorLog.*|ErrorLog /dev/stderr|' /etc/apache2/apache2.conf; \
    else \
	echo ErrorLog /dev/stderr >>/etc/apache2/apache2.conf; \
    fi \
    && if grep TransferLog /etc/apache2/apache2.conf >/dev/null; then \
	sed -i 's|TransferLog.*|TransferLog /dev/stdout|' /etc/apache2/apache2.conf; \
    else \
	echo TransferLog /dev/stdout >>/etc/apache2/apache2.conf; \
    fi \
    && if test "$DEBUG"; then \
	sed -i 's|LogLevel warn|LogLevel debug|' /etc/apache2/apache2.conf; \
    fi \
    && mkdir -p /vhosts \
    && mv /do-ssl.conf /no-ssl.conf /kindof-ssl.conf /etc/apache2/ \
    && mv /custom-log-fmt.conf /remoteip.conf /etc/apache2/conf-available/ \
    && a2enmod alias remoteip rewrite ssl headers perl status \
    && a2enconf remoteip custom-log-fmt \
    && mv /lemon-sso.sh /nsswrapper.sh /setupvhosts.sh /reset-tls.sh \
	/usr/local/bin/ \
    && cp -p /etc/lemonldap-ng/lemonldap-ng.ini \
	/root/original-lemonldap-ng.ini \
    && echo "# Fixing permissions" \
    && for dir in /var/www/html /etc/apache2/ssl /etc/apache2/sites-enabled \
	    /etc/lemonldap-ng; \
	do \
	    rm -rf "$dir" \
	    && mkdir -p "$dir" \
	    && ( chown -R 1001:root "$dir" || echo nevermind ) \
	    && ( chmod -R g=u "$dir" || echo nevermind ); \
	done \
    && for dir in /etc/apache2/conf.d /etc/apache2/mods-enabled \
	    /etc/apache2/mods-available; \
	do \
	    mkdir -p "$dir" \
	    && chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "Apache OK" >/var/www/html/index.html \
    && for i in /var/www/html/index.html /etc/ssl \
	    /usr/share/ca-certificates /usr/local/share/ca-certificates; \
	do \
	    chown -R 1001:root "$i" \
	    && chmod -R g=u "$i"; \
	done \
    && chown -R 1001:root /tmp /etc/apache2/apache2.conf \
    && ( chmod -R g=u /run /tmp || echo nevermind ) \
    && if test "$DUMMY_DH_SIZE" -ge 0 >/dev/null 2>&1; then \
	echo "# Creating Dummy DH Params - this will take some time" \
	&& openssl dhparam -out /etc/ssl/dhparam.pem $DUMMY_DH_SIZE \
	    >/dev/null 2>&1 \
	&& chmod 0444 /etc/ssl/dhparam.pem \
	&& chown 0:0 /etc/ssl/dhparam.pem \
	&& ln -sf /etc/ssl/dhparam.pem /etc/ssl/certs/; \
    fi \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge gnupg build-essential cpanminus \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /etc/apache2/ports.conf /usr/share/man /usr/share/doc /.cpan \
	/var/lib/apt/lists/* /etc/apache2/sites-enabled/*default* /root/.cpan \
	/lemonldap-ng.key \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT [ "dumb-init", "--", "/run-apache.sh" ]
USER 1001
WORKDIR /var/www/html
